Spring 2020 CS445 HW1
Gyucheon Heo
A20393055

1. Install openjdk-8-default and maven
$>sudo apt-get update -y && sudo apt-get install openjdk-8-jdk maven git

2. Clone the repository
$>git clone http://bitbucket.org/gheo1/cs445-hw

3. Change the directory, where pom.xml exists
$>cd cs445-hw/hw1

4. Install dependencies
$>mvn clean install

5. Run Unit Test
$>mvn test

6. Run TestCreature
$>mvn exec:java

