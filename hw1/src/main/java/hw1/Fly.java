package hw1;

public class Fly extends Creature implements Flyer {

	public Fly(String name) {
		super(name);
	}
	
	@Override
	public void eat(Thing aThing) {
		if(aThing instanceof Creature) {
			System.out.print(super.toString() + " won't eat a " + aThing+".");
		} else {
			super.eat(aThing);
		}
	}
	public void fly() {
		System.out.print(super.toString() + " is buzzing around in flight.");
	}

	@Override
	public void move() {
		this.fly();
	}

}
