package hw1;


public abstract class Creature extends Thing {
	String thingInStomach;
	public Creature(String string) {
		super(string);
	}

	public void eat(Thing aThing) {
		System.out.print(super.toString() + " has just eaten a " + aThing+".");
		thingInStomach = aThing.getName();
	}
	
	public abstract void move();
	
	public void WhatDidYouEat() {
		if(this.thingInStomach == null) {
			System.out.print(super.toString() + " has had nothing to eat!");
		} else {
			System.out.print(super.toString() + " has eaten a "+ this.thingInStomach);
		}
	}

}
