package hw1;

public class TestCreature {
	static int CREATURE_COUNT=6;
	static int THING_COUNT=10;
	
	public static void main(String[] args) {
			Thing[] things = new Thing[THING_COUNT];
			Creature[] creatures = new Creature[CREATURE_COUNT];
			
			Creature bat1 = new Bat("bat1");
			creatures[0] = bat1;
			Creature ant1 = new Ant("ant1");
			creatures[1] = ant1;
			Creature fly1 = new Fly("fly1");
			creatures[2] = fly1;
			Creature tiger1 = new Tiger("Tigger, Pooh's Friend");
			creatures[3] = tiger1;
			Creature bat2 = new Bat("bat2");
			creatures[4] = bat2;
			Creature ant2 = new Ant("ant2");
			creatures[5] = ant2;
			
			Thing thing1 = new Thing("Banana");
			Thing thing2 = new Thing("Apple");
			Thing thing3 = new Thing("Avocado");
			Thing thing4 = new Thing("Pepper");
			things[0] = thing1;
			things[1] = thing2;
			things[2] = thing3;
			things[3] = thing4;
			things[4] = creatures[0];
			things[5] = creatures[1];
			things[6] = creatures[2];
			things[7] = creatures[3];
			things[8] = creatures[4];
			things[9] = creatures[5];
			
			
			System.out.println("Things:\n");
			
			for(Thing thing : things) {
				System.out.println(thing);
			}
			
			System.out.println("");
			
			System.out.println("Creatures:\n");
			
			for(Creature creature : creatures) {
				System.out.println(creature);
			}
			
			for(Creature creature : creatures) {
				creature.move();
				System.out.println("");
			}
			System.out.println("");
	}
}
