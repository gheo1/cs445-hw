package hw1;

public class Ant extends Creature {
	public Ant(String name) {
		super(name);
	}
	@Override
	public void move() {
		System.out.print(super.toString() + " is crawling around.");
	}

}
