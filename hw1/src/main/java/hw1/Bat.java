package hw1;

public class Bat extends Creature implements Flyer{
	public Bat(String name) {
		super(name);
	}
	@Override
	public void move() {
		this.fly();
	}
	
	@Override
	public void eat(Thing aThing) {
		if(aThing.getClass().getSimpleName().equals("Thing")) {
			System.out.print(super.toString() + " won't eat a " + aThing);
			return;
		}
		else {
			super.eat(aThing);
		}

	}
	
	public void fly() {
		System.out.print(super.toString() + " is swooping through the dark.");
	}

}
