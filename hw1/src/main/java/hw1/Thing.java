package hw1;

public class Thing {
	
	private final String name;
	
	public Thing(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String toString() {
		if(this.getClass().getSimpleName().equals("Thing")) {
			return this.name;
		} else {
			return this.name + " " + this.getClass().getSimpleName();
		}
	}
}
