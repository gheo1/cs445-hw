package hw1;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class TigerTest {

	private Tiger tiger;
	private ByteArrayOutputStream outContent;	
	
	@Before
	public void setUp() {
		tiger = new Tiger("King");
		outContent = new ByteArrayOutputStream();
		
	}
	
	@Test
	public void createTiger() {
		assertEquals("King", tiger.getName());
	}
	
	@Test
	public void TigerMoves_ShouldPrint_name_class_has_just_pounced() {
		System.setOut(new PrintStream(outContent));
		tiger.move();
		assertEquals("King Tiger has just pounced.", outContent.toString());
	}
}
