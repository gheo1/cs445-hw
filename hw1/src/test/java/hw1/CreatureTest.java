package hw1;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class CreatureTest {

	private Creature creature;
	private ByteArrayOutputStream outContent;
	
	@Before
	public void setUp() {
		creature = new MockingCreature("test_creature");
		outContent = new ByteArrayOutputStream();
	}
	@Test
	public void createCreature() {
		assertEquals("test_creature", creature.getName());
	}
	
	@Test
	public void eatThing_ByDefaultEatWhateverTheyAreToldAndPrintMessage() {
		System.setOut(new PrintStream(outContent));
		creature.eat(new Thing("aThing"));
		assertEquals("test_creature MockingCreature has just eaten a aThing.", outContent.toString());
		assertEquals("aThing", creature.thingInStomach);
	}
	
	@Test
	public void ateNothing_shouldPrint_name_class_has_had_nothing_to_eat() {
		System.setOut(new PrintStream(outContent));
		creature.WhatDidYouEat();
		assertEquals("test_creature MockingCreature has had nothing to eat!", outContent.toString() );
	}
	
	@Test
	public void ateSomething_shouldPrint_creatureName_className_has_eaten_content_of_stomach() {
		creature.eat(new Thing("aThing"));
		System.setOut(new PrintStream(outContent));
		creature.WhatDidYouEat();
		assertEquals("test_creature MockingCreature has eaten a aThing", outContent.toString());
	}
	
}
