package hw1;

import static org.junit.Assert.assertEquals;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.junit.Before;
import org.junit.Test;

public class BatTest {
	private Bat bat;
	private ByteArrayOutputStream outContent;	
	@Before
	public void setUp() {
		bat = new Bat("Batman");
		outContent = new ByteArrayOutputStream();
	}
	
	@Test
	public void createBat() {
		assertEquals("Batman", bat.getName());
	}
	
	@Test
	public void batEatsNotInstanceOfCreature_shouldprint_name_class_wont_eat_a_aThing() {
		System.setOut(new PrintStream(outContent));
		bat.eat(new Thing("DONOTEAT"));
		assertEquals("Batman Bat won't eat a DONOTEAT", outContent.toString());

		
	}
	
	@Test
	public void batEatsWhatBatCan_should_print_() {
		Creature c = new Ant("Yummy!");
		System.setOut(new PrintStream(outContent));
		bat.eat(c);
		assertEquals("Batman Bat has just eaten a Yummy! Ant.", outContent.toString());
	}
	
	
	@Test
	public void batMoves_should_call_fly() {
		System.setOut(new PrintStream(outContent));	
		bat.move();
		assertEquals("Batman Bat is swooping through the dark.", outContent.toString());
	}
	
	@Test
	public void batFlies_should_print_name_class_is_swooping_through_the_dark() {
		System.setOut(new PrintStream(outContent));
		bat.fly();
		assertEquals("Batman Bat is swooping through the dark.", outContent.toString());
	}
}
