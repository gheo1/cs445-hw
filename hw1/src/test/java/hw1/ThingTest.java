package hw1;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class ThingTest {
	private Thing thing;
	
	@Before
	public void setUp() {
		thing = new Thing("test_thing");
	}
	@Test
	public void createThing() {
		assertEquals("test_thing", thing.getName());
	}
	
	@Test
	public void toStringShouldReturnJustName() {
		assertEquals(thing.getName(), thing.toString());
	}
	
}
