package hw1;

import static org.junit.Assert.assertEquals;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class FlyTest {

	private Fly fly;
	private ByteArrayOutputStream outContent;	
	
	@Before
	public void setUp() {
		fly = new Fly("ffly");
		outContent = new ByteArrayOutputStream();
	}
	
	@Test
	public void createFly() {
		assertEquals("ffly", fly.getName());
	}
	
	@Test
	public void flyEatsOnlyThings() {
		System.setOut(new PrintStream(outContent));
		fly.eat(new Thing("yummy"));
		assertEquals("ffly Fly has just eaten a yummy.", outContent.toString());
	}
	
	@Test
	public void flyNotEatCreature() {
		Creature c = new Ant("Not yummy");
		System.setOut(new PrintStream(outContent));
		fly.eat(c);
		assertEquals("ffly Fly won't eat a Not yummy Ant.", outContent.toString());
	}
	
	@Test
	public void flyFlies_ShouldPrint_name_class_is_buzzing_around_in_flight() {
		System.setOut(new PrintStream(outContent));
		fly.fly();
		assertEquals("ffly Fly is buzzing around in flight.", outContent.toString());
	}
	
	@Test
	public void flyMoves_should_fly() {
		System.setOut(new PrintStream(outContent));
		fly.move();
		assertEquals("ffly Fly is buzzing around in flight.", outContent.toString());
	}
}
