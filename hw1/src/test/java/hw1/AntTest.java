package hw1;

import static org.junit.Assert.assertEquals;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class AntTest {

	private Ant ant;
	private ByteArrayOutputStream outContent;	
	@Before
	public void setUp() {
		ant = new Ant("Ant");
		outContent = new ByteArrayOutputStream();
	}
	
	@Test
	public void createAnt() {
		assertEquals("Ant", ant.getName());
	}
	@Test
	public void AntMove_ShouldPrint_name_class_is_crawling_around() {
		System.setOut(new PrintStream(outContent));
		ant.move();
		assertEquals("Ant Ant is crawling around.", outContent.toString());
	}
}
